import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class J2048 {

    public static void main(String[] arg){


        
        int[][] element = new int[4][4];
        
        start(element);
        
        getPoint(element);



    } // end main;

    public static void start(int[][] element)
    {
        Scanner input = new Scanner(System.in);

        while(true)
        {
            clearScreen();
            addEleman(element);    // put 2 or 4 in random place (sides)
            print(element);

            if(ifOver(element))
                break;


            char action = input.next().charAt(0);

            switch(action)
            {
                case 'w' : actionInDirect(element , "up"  );   break;

                case 's' : actionDirect(element , "down"  );   break;

                case 'd' : actionDirect(element , "right" );   break;

                case 'a' : actionInDirect(element , "left");   break;
            } 
            
        }

        input.close();

    } //end Start methode


    public static void print(int[][] arr)
    {
        int[][] eleman = arr ;

        
        System.out.println("\n");

         for(int i=0 ; i<4 ; i++)
         {  
             
            System.out.print("| ");

              for(int j=0 ; j<4 ; j++){
                if(eleman[i][j]==0)                                   // print white space
                     System.out.print(" " + "\t" + "| ");
                else
                     System.out.print(eleman[i][j] + "\t" + "| ");}


            System.out.println("\n\n");
         }
    }//end print




    public static void addEleman(int[][] elements)
    {
        if(getEmptyElements(elements).size() == 0 )
            return ;

        int c = getRandomElement(getEmptyElements(elements)) ;

        int i = c/10;
        int j = c%10;

        elements[i][j] = get_random_2_or_4() ;

    } //end addEleman



    public static int getRandomElement(ArrayList<Integer> list) {

            Random rand = new Random();
            return list.get(rand.nextInt(list.size()));
            
    } // end getRandomElement


    public static int get_random_2_or_4() {

            Random rand = new Random();

            ArrayList<Integer> nums = new ArrayList<Integer>();

            for(int i = 0 ; i<5 ; i++)          //  The probability of 2 is five times more than 4
                nums.add(2);

                nums.add(4);

            return nums.get(rand.nextInt(nums.size())) ;

    } // end get_random_2_or_4
    

    public static ArrayList<Integer> getEmptyElements(int[][] element){

        ArrayList<Integer> empties = new ArrayList<Integer>();


        for(int i=0 ; i<4 ; i++)
        {   
            for(int j = 0 ; j<4 ; j++)
            {
               
                if((i==1 && j==1) || (i==1 & j==2) || (i==2 && j==1) || (i==2 && j==2))  // dont put 2 in the middle blocks
                     continue;

                if(element[i][j] == 0)  // just the empty ones
                  {
                        int c = i*10 + j;
                        empties.add(c);
                  }

            }
        }

            return empties ;
    } // end getEmptyEliment


    public static boolean ifOver(int[][] element){


        ArrayList<Integer> emptyOnes = getEmptyElements(element) ;

        if(emptyOnes.size()>=1 || hasSame(element))
            return false;

        else   
            return true;


    } //end ifOver

    public static boolean hasSame(int[][] element){

        for(int i = 0 ; i<4 ; i++)
        {
            for(int j=0 ; j<3 ; j++)
            {
                if(element[i][j]!=0 ) //skip the empty ones
                    if(element[i][j]==element[i][j+1])  //check the rows
                          return true;

                
                if(element[j][i]!=0 )   
                    if(element[j][i]==element[j+1][i])  //check the collumns
                          return true;
            }
        }

        return false;
    } //end hasSame


    public static void actionDirect(int[][] element , String side) // right & down
    {
       pushDirect(element, side);

        for(int i = 0 ; i<4 ; i++ )
        {
            for(int j = 0 ; j<3 ;  )
            {
                if(side.equals("right"))
                        if(element[i][j]==element[i][j+1])
                        {
                            element[i][j+1] *=2 ;

                            element[i][j] = 0 ;

                            j+=2;
                        }
                        else
                        {
                            j++;
                        }

                if(side.equals("down"))
                        if(element[j][i]==element[j+1][i])
                        {
                            element[j+1][i] *=2 ;

                            element[j][i] = 0 ;

                            j+=2;
                        }
                        else
                        {
                            j++;
                        }
            }


        }

       pushDirect(element, side);

    } //end actionDirect // right & down
   

    public static void pushDirect(int[][] element , String side)  
    {
        int[] temp = new int[4];
        int j ;

        for(int i = 0 ; i<4 ; i++)
        {
            
            for( j = 0 ; j<4 ; j++)
            {
                if(side.equals("right"))
                    {
                        temp[j] = element[i][j] ;
                        element[i][j] = 0;
                    }

                if(side.equals("down"))
                    {
                        temp[j] = element[j][i] ;
                        element[j][i] = 0;
                    }
            }
               
            int p =3 ;

            for( j = 3 ; j>=0 ; j--)
            {
                if(side.equals("right"))
                    if(temp[j]!=0)
                    {
                        element[i][p] = temp[j];
                        p--;
                    }


                if(side.equals("down"))
                    if(temp[j]!=0)
                    {
                        element[p][i] = temp[j];
                        p--;
                    }


            }
              
            
        }

    } //end pushDirect

    public static void actionInDirect(int[][] element , String side)   // left & up
    {
         pushInDirect(element, side);

         for(int i = 0 ; i<4 ; i++ )
         {
            for(int j = 3 ; j>0 ;  )
            {
                if(side.equals("left"))
                    if(element[i][j]==element[i][j-1])
                    {
                        element[i][j-1] *=2 ;

                        element[i][j] = 0 ;

                        j-=2;
                    }
                    else
                    {
                        j--;
                    }



                if(side.equals("up"))
                    if(element[j][i]==element[j-1][i])
                    {
                        element[j-1][i] *=2 ;

                        element[j][i] = 0 ;

                        j-=2;
                    }
                    else
                    {
                        j--;
                    }

            }


        }

      pushInDirect(element, side);

    } //end actionInDirect (left  & up )
   

    public static void pushInDirect(int[][] element , String side)
    {
        int[] temp = new int[4];
        int j ;

        for(int i = 0 ; i<4 ; i++)
        {
            
            for( j = 0 ; j<4 ; j++)
            {
                
                if(side.equals("left"))
                 {
                     temp[j] = element[i][j] ;
                     element[i][j] = 0;
                 }

                 if(side.equals("up"))
                 {
                    temp[j] = element[j][i] ;
                    element[j][i] = 0;
                 }
                 
            }
                
            int p =0 ;

            for( j = 0 ; j<4 ; j++)
            {
                if(side.equals("left"))
                    if(temp[j]!=0)
                    {
                        element[i][p] = temp[j];
                        p++;
                    }

                if(side.equals("up"))
                    if(temp[j]!=0)
                    {
                        element[p][i] = temp[j];
                        p++;
                    }

            }
              
            
        }

    } //end pushInDirect


    public static void clearScreen()
     {  

        System.out.print("\033[H\033[2J");  
     
        System.out.flush();  
     
     } // end clearScreen


    public static void getPoint(int[][] element)
    {
        int point = 0;

        for(int i = 0 ; i<4 ; i++)
            for(int j=0 ; j<4 ; j++)
                point += element[i][j];


        System.out.println("No other movement is allowed . ");
        System.out.printf("\nYou got %d points . " , point);
                
    } //end getPoint
    
} // bye 
